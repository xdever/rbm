#ifndef MNIST_H
#define MNIST_H

#include <string>
#include <stdint.h>
#include <vector>
#include "Database.h"

class MNIST : public Database
{
private:
    uint32_t cnt;
    uint8_t *images;
    uint8_t *labels;

    std::vector <uint8_t*>  samplesByCategory[10];

    void clear();
    void loadImage(const std::string &imageFile);
    void loadLabels(const std::string &labelFile);

    static void fixEndian(uint32_t *data, int cnt);
    void initCategories();
public:
    MNIST();
    ~MNIST();

    uint32_t count() const;
    void open(const std::string &imageFile, const std::string &labelFile);


    DatabaseSample getSampleFromAllCategories(int samplePerCategory) const;
    uint32_t categoryCount() const;

    DatabaseSample getSequential(int index, int numSamples) const;
};

#endif // MNIST_H
