#ifndef DATABASE_H
#define DATABASE_H

#include <stdint.h>
#include "Matrix.h"
#include <vector>

struct DatabaseSample{
    Matrix imageData;
    std::vector <int> labels;
};

class Database{
    public:
        virtual uint32_t count() const=0;
        virtual uint32_t categoryCount() const=0;
        virtual DatabaseSample getSampleFromAllCategories(int samplePerCategory) const=0;

        virtual DatabaseSample getSequential(int index, int numSamples) const=0;
};

#endif // DATABASE_H

