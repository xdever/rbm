#include "MNIST.h"
#include <QFile>
#include <iostream>
#include <stdlib.h>
#include <arpa/inet.h>
#include <assert.h>
#include <string.h>


MNIST::MNIST()
{
    cnt=0;
    images=NULL;
    labels=NULL;
}


void MNIST::clear(){
    if (images)
        delete [] images;

    if (labels)
        delete [] labels;

    cnt=0;
}

MNIST::~MNIST()
{
    clear();
}

uint32_t MNIST::count() const{
    return cnt;
}

void MNIST::fixEndian(uint32_t *data, int cnt){
    for (int a=0; a<cnt; a++){
        data[a]=ntohl(data[a]);
    }
}

void MNIST::loadImage(const std::string &imageFile){
    struct imageHeader{
        uint32_t magic;
        uint32_t cnt;
        uint32_t w;
        uint32_t h;
    } __attribute__((packed));

    QFile input(imageFile.c_str());
    if (!input.open(QFile::ReadOnly)){
        std::cerr<<"Failed to open file '"<<imageFile<<"'!"<<std::endl;
        exit(-1);
    }

    imageHeader header;
    assert(input.read((char*)&header, sizeof(header))==sizeof(header));

    fixEndian((uint32_t*)&header, sizeof(header)/4);

    assert(header.magic==0x00000803);
    assert(header.w==28 && header.h==28);

    cnt=header.cnt;
    int size=header.h*header.w*header.cnt;
    images=new uint8_t[size];

    assert(input.read((char*)images, size)==size);

    std::cout<<"Loaded "<<cnt<<" images"<<std::endl;

    input.close();
}

void MNIST::loadLabels(const std::string &labelFile){
    struct labelHeader{
        uint32_t magic;
        uint32_t cnt;
    } __attribute__((packed));

    QFile input(labelFile.c_str());
    if (!input.open(QFile::ReadOnly)){
        std::cerr<<"Failed to open file '"<<labelFile<<"'!"<<std::endl;
        exit(-1);
    }

    labelHeader header;
    assert(input.read((char*)&header, sizeof(header))==sizeof(header));

    fixEndian((uint32_t*)&header, sizeof(header)/4);

    assert(header.magic==0x00000801);
    if (header.cnt!=cnt){
        std::cerr<<"Image and label file size doesn't match ("<<header.cnt<<"!="<<cnt<<")"<<std::endl;
        exit(-1);
    }

    labels=new uint8_t[cnt];
    assert(input.read((char*)labels, cnt)==cnt);

    input.close();
}

void MNIST::open(const std::string &imageFile, const std::string &labelFile){
    loadImage(imageFile);
    loadLabels(labelFile);
    initCategories();
}

void MNIST::initCategories(){
    for (int a=0; a<10; a++)
        samplesByCategory[a].clear();

    for (uint32_t a=0; a<cnt; a++){
        samplesByCategory[labels[a]].push_back(&this->images[a*28*28]);
    }
}


DatabaseSample MNIST::getSampleFromAllCategories(int samplePerCategory) const{
    DatabaseSample result;
    result.imageData=Matrix(samplePerCategory*10, 28*28);

    int currRow=0;
    for (int num=0; num<10; num++){
        for (int sample=0; sample<samplePerCategory; sample++){
            uint8_t *img=samplesByCategory[num][rand() % samplesByCategory[num].size()];
            for (int pix=0; pix<28*28; pix++){
                 result.imageData(currRow, pix)=((float)img[pix])/255.0;
            }

            result.labels.push_back(num);
            currRow++;
        }
    }

    return result;
}

uint32_t MNIST::categoryCount() const{
    return 10;
}

DatabaseSample MNIST::getSequential(int index, int numSamples) const{
    DatabaseSample result;
    result.imageData=Matrix(numSamples, 28*28);

    for (int sample=0; sample<numSamples; sample++){
        uint8_t *img=&images[28*28*(index+sample)];
        for (int pix=0; pix<28*28; pix++){
             result.imageData(sample, pix)=((float)img[pix]) /255.0 ;
        }

        result.labels.push_back(labels[index+sample]);
    }

    return result;
}
