#include "PaintableImage.h"
#include <iostream>
#include <QPainter>

PaintableImage::PaintableImage(QWidget *parent) : QLabel(parent)
{

}

void PaintableImage::mousePressEvent(QMouseEvent * event){
    int x=event->x();
    int y=event->y();

    if (x >= this->width() || y>=this->height() || x<0 || y<0){
        return;
    }

    lastX=x;
    lastY=y;
}

void PaintableImage::lineto(int x, int y){
    if (x >= this->width() || y>=this->height() || x<0 || y<0){
        return;
    }

    QPainter painter;
    painter.begin(&image);
    painter.setPen(QPen(QColor(255, 255, 255), 16, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter.drawLine(lastX,lastY, x, y);
    painter.end();

    lastX=x;
    lastY=y;

    refreshImage();
}

void PaintableImage::mouseMoveEvent(QMouseEvent * event){
    lineto(event->x(), event->y());
}

void PaintableImage::clear(){
    image.fill(qRgb(0,0,0));
    refreshImage();
}

void PaintableImage::resizeEvent(QResizeEvent * event){
    image=QImage(event->size(), QImage::Format_RGB32);
    clear();
}

void PaintableImage::refreshImage(){
    setPixmap(QPixmap::fromImage(image));
}

void PaintableImage::mouseReleaseEvent(QMouseEvent * event){
    lineto(event->x(), event->y());
    emit imageChanged(image);
}
