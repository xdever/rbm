#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "MNIST.h"
#include "DBN.h"
#include "TrainThread.h"
#include "TestThread.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_sampleChooser_valueChanged(int arg1);

    void onStateInfo(std::vector<QImage> img, int round, float error);
    void onTrainInfo(int progress, float accuracy);
    void onImageDrawn(QImage image);

    void on_createFromDbMenu_triggered();

    void on_dreamRandomMenu_triggered();

    void on_saveMenu_triggered();

    void on_loadMenu_triggered();

    void on_trainStartMenu_triggered();

    void on_resetStateMenu_triggered();

    void on_dreamBackMenu_triggered();

    void on_layerSelector_valueChanged(int arg1);

    void on_learningRateSelector_valueChanged(float arg1);

    void on_dreamCategories_triggered();

    void on_testStartMenu_triggered();

    void on_resetImageBtn_clicked();

private:
    Ui::MainWindow *ui;
    MNIST trainDb;
    MNIST testDb;
    GenerativeNetwork *network;
    TrainThread *trainer;
    TestThread *tester;

    std::vector <QImage> weights;

    bool oldTrainState;

    QPalette paletteDefault;

    void pushTrainState();
    void popTrainState();

    void setLabelImage(QLabel *label, QImage &image);


    void showPreview(int index);
    void showDream(int index);
    void showWeights();
    void startTrain();
    void stopTrain();
    void showNetworkData();
    void resetState();

    static void getImageBounds(const QImage &image, int &minX, int &maxX, int &minY, int &maxY);
    static QImage getActiveArea(const QImage &src);
    static QImage minstPreprocess(const QImage &image);
    static void centroid(const QImage &image, int &centerX, int &centerY);
};

#endif // MAINWINDOW_H
