#include "RBMVisualizer.h"
#include <iostream>
#include "math.h"

RBMVisualizer::RBMVisualizer()
{

}

RBMVisualizer::~RBMVisualizer()
{

}

QImage RBMVisualizer::plotWeights(int nVisible, int nHidden, const Matrix &weights, int nInRow, int weightImageW, int padding, bool globalNormalize){
    if (nInRow>nHidden)
        nInRow=nHidden;


    int weightImageH=ceil(((float)nVisible)/weightImageW);

    int w=nInRow*(weightImageW+padding)+padding;
    int h=ceil(((float)nHidden)/nInRow)*(weightImageH+padding)+padding;
    QImage result(w,h, QImage::Format_RGB32);

    result.fill(0);



    float normMin;
    float normMax;

    if (globalNormalize){
        weights.minMax(normMin, normMax);
    }

    for (int a=0; a<nHidden; a++){
        if (!globalNormalize){
            weights.minMaxCol(a, normMin, normMax);
        }

        int startX=padding+(weightImageW+padding)*(a % nInRow);
        int startY=padding+(weightImageH+padding)*floor(a / nInRow);

        int xRel=0;
        int yRel=0;

        for (int v=0; v<nVisible; v++){
            uint8_t pixel=(weights(v,a) - normMin)/(normMax-normMin)*255;
           // std::cout<<"Pixel (xRel: "<<xRel<<", yRel: "<<yRel<<"): "<<(uint32_t)pixel<<std::endl;
            result.setPixel(startX+xRel, startY+yRel, qRgb(pixel,pixel,pixel));

            xRel++;
            if (xRel>=weightImageW){
                xRel=0;
                yRel++;
            }
        }
    }

    return result;
}

std::vector<QImage> RBMVisualizer::plotWeights(const RBM &rbm, int nInRow, int weightImageW, int padding, bool globalNormalize){
    std::vector <QImage> result;
    result.push_back(plotWeights(rbm.state.nVisible, rbm.state.nHidden, rbm.state.weights, nInRow,weightImageW, padding, globalNormalize));
    return result;
}

std::vector<QImage> RBMVisualizer::plotWeights(const DBN &rbm, int nInRow, int weightImageW, int padding, bool globalNormalize){
    std::vector <QImage> result;
    int nVisible=rbm.layers[0]->state.nVisible;

    Matrix w=rbm.layers[0]->state.weights;
    result.push_back(plotWeights(nVisible, rbm.layers[0]->state.nHidden, w, nInRow,weightImageW, padding, globalNormalize));

    for (int l=1; l<rbm.layerCnt; l++){
        if (rbm.classificationCategories!=0 && l==rbm.layerCnt-1){
            Matrix &m=rbm.layers[l]->state.weights;
            w = w * m.submatrix(0,0,m.rows()-rbm.classificationCategories, m.cols());
        } else {
            w = w * rbm.layers[l]->state.weights;
        }
        result.push_back(plotWeights(nVisible, rbm.layers[l]->state.nHidden, w, nInRow,weightImageW, padding, globalNormalize));
    }

    return result;
}

void RBMVisualizer::plotMatrixColumn(QImage &dest, int x, int y, int padding, int weightImgW, float min, float max, const Matrix &mat){
    int ypos=y;
    for (int r=0; r<mat.rows(); r++){
        int xrel=0;
        for (int p=0; p<mat.cols(); p++){
            uint8_t pixel=255.0*(mat(r,p)-min)/(max-min);

            dest.setPixel(x+xrel, ypos, qRgb(pixel,pixel,pixel));

            xrel++;
            if (xrel==weightImgW){
                xrel=0;
                ypos++;
            }
        }

        ypos+=padding;
    }
}

/**
 * @brief Dream based on given sampler.
 *
 * @param sampler network sampler.
 * @param input list of input images.
 * @param weightImgW the width of one image.
 * @param padding padding bethween images.
 * @param the distance bethween steps.
 * @param nSteps the number of dreams to generate.
 * @return rendered images.
 */

QImage RBMVisualizer::dreamAndRender(std::shared_ptr<GenerativeNetworkSampler> sampler, Matrix input,int weightImgW, int padding,  int stepSize, int nSteps){
    const int nSamples=nSteps;

    int weightImgH=ceil(((float)sampler->layerSize())/weightImgW);

    auto imgW=(nSamples+1)*(weightImgW+padding)+2*padding;
    auto imgH=input.rows()*(weightImgH+padding)+padding;

    QImage result(imgW, imgH, QImage::Format_RGB32);
    result.fill(0);

    //Render input images
    plotMatrixColumn(result, padding, padding, padding, weightImgW, 0, 1, input);


    for (int step=0; step<nSamples; step++){
        Matrix visibleProbability=sampler->sample(stepSize);

        plotMatrixColumn(result, 2*padding+(padding+weightImgW)*(step+1), padding, padding, weightImgW, 0, 1, visibleProbability);
    }


    return result;
}

/**
 * @brief Dream based on samples from database (reconstruction).
 *
 * @param rbm the network to generate from.
 * @param db databse to get sample images from.
 * @param sampleDistance the distance of rendered samples (the number of gibbs samples bethween them).
 * @param weightImgW the width of one image.
 * @param padding padding bethween images.
 * @return rendered images.
 */
QImage RBMVisualizer::dream(const GenerativeNetwork &rbm, const Database &db, int sampleDistance, int weightImgW, int padding){
    DatabaseSample samples=db.getSampleFromAllCategories(1);

    auto sampler=rbm.startSampling(samples.imageData, false);

    return dreamAndRender(sampler, samples.imageData, weightImgW, padding, sampleDistance,20);
}

/**
 * @brief Dream from random data.
 *
 * @param rbm the network to generate from.
 * @param sampleDistance the distance of rendered samples (the number of gibbs samples bethween them).
 * @param cnt the number of images to dream.
 * @param weightImgW the width of one image.
 * @param padding padding bethween images.
 * @return rendered images.
 */
QImage RBMVisualizer::dream(const GenerativeNetwork &rbm, int sampleDistance, int cnt, int weightImgW, int padding, bool startFromBack){
    Matrix data;

    auto thresholdZero = [](float d){
        return d>0.0f ? 1.0f : 0.0f;
    };

    if (startFromBack){
        data=Matrix::UniformRandom(cnt,rbm.getHiddenSize(),-1,1).elementwise(thresholdZero);
    } else {
        data=Matrix::UniformRandom(cnt,rbm.getVisibleSize(),-1,1).elementwise(thresholdZero);
    }

    auto sampler=rbm.startSampling(data, startFromBack);

    if (startFromBack){
        data=sampler->sample(1);
    }

    return dreamAndRender(sampler, data, weightImgW, padding, sampleDistance,cnt);
}

/**
 * @brief Dream from every category.
 *
 * @param rbm the network to generate from.
 * @param sampleDistance the distance of rendered samples (the number of gibbs samples bethween them).
 * @param weightImgW the width of one image.
 * @param padding padding bethween images.
 * @return rendered images.
 */
QImage RBMVisualizer::dreamFromCategories(const GenerativeNetwork &rbm, int sampleDistance, int weightImgW, int padding){
    auto sampler=rbm.startCategorySampling();
    return dreamAndRender(sampler, sampler->sample(1), weightImgW, padding, sampleDistance, rbm.getCategoryCount());
}

