#ifndef PAINTABLEIMAGE_H
#define PAINTABLEIMAGE_H

#include <QLabel>
#include <QMouseEvent>

/**
 * @brief Image on which you can draw with
 * the mouse.
 *
 */
class PaintableImage : public QLabel
{
    Q_OBJECT
private:
    QImage image;
    int lastX;
    int lastY;

    void refreshImage();
    void lineto(int x, int y);
public:
    explicit PaintableImage(QWidget *parent = 0);
    void clear();
protected:
    void mouseMoveEvent(QMouseEvent * event);
    void resizeEvent(QResizeEvent * event);
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);

signals:
    void imageChanged(QImage image);
public slots:
};

#endif // PAINTABLEIMAGE_H
