#ifndef RBMVISUALIZER_H
#define RBMVISUALIZER_H

#include "RBM.h"
#include "GenerativeNetwork.h"
#include "DBN.h"
#include <vector>

class RBMVisualizer
{
private:
    RBMVisualizer();
    static void plotMatrixColumn(QImage &dest, int x, int y, int padding, int weightImgW, float min, float max, const Matrix &mat);
    static QImage dreamAndRender( std::shared_ptr<GenerativeNetworkSampler> sampler, Matrix input,int weightImgW, int padding, int stepSize, int nSteps);

    static QImage plotWeights(int nVisible, int nHidden, const Matrix &weights, int nInRow, int weightImageW, int padding=2, bool globalNormalize=true);


public:
    ~RBMVisualizer();
    static std::vector<QImage> plotWeights(const DBN &rbm, int nInRow, int weightImageW, int padding=2, bool globalNormalize=true);
    static std::vector<QImage> plotWeights(const RBM &rbm, int nInRow, int weightImageW, int padding=2, bool globalNormalize=true);
    static QImage dream(const GenerativeNetwork &rbm, const Database &db, int sampleDistance=5, int weightImgW=28, int padding=2);
    static QImage dream(const GenerativeNetwork &rbm, int sampleDistance=5, int cnt=10, int weightImgW=28, int padding=2, bool startFromBack=false);
    static QImage dreamFromCategories(const GenerativeNetwork &rbm, int sampleDistance=5, int weightImgW=28, int padding=2);
};

#endif // RBMVISUALIZER_H
