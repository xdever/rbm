#include <QImage>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QFileDialog>
#include "RBMVisualizer.h"
#include <QPainter>

#define RENDER_CNT_PER_ROW 20

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    network = new DBN(3, 10, 28*28, 64, 500, 2000);
   // network = new RBM(28*28,200);
    trainer = new TrainThread(network, &trainDb);
    tester = new TestThread(network, &testDb);

    ui->setupUi(this);

    trainDb.open(":/Datasets/train-images.idx3-ubyte", ":/Datasets/train-labels.idx1-ubyte");
    testDb.open(":/Datasets/t10k-images.idx3-ubyte", ":/Datasets/t10k-labels.idx1-ubyte");

    ui->sampleChooser->setRange(0,trainDb.count()-1);
    showPreview(0);

    resetState();

    qRegisterMetaType<std::vector<QImage> >("std::vector<QImage>");

    connect(trainer, SIGNAL(stateInfo(std::vector<QImage>,int, float)),
             this, SLOT(onStateInfo(std::vector<QImage>,int, float)));

    connect(tester, SIGNAL(stateInfo(int, float)),
             this, SLOT(onTrainInfo(int, float)));

    connect(ui->paintLabel, SIGNAL(imageChanged(QImage)),
             this, SLOT(onImageDrawn(QImage)));

    paletteDefault = ui->learningRateSelector->palette();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete trainer;
    delete tester;
    delete network;
}

void MainWindow::showWeights(){
    ui->layerSelector->setRange(0, weights.size()-1);
    setLabelImage(ui->weightLabel, weights[ui->layerSelector->value()]);
}

void MainWindow::showPreview(int index){
    DatabaseSample sample=trainDb.getSequential(index,1);


    QImage image(28,28, QImage::Format_RGB32);
    //Fill image by hand because older QT doesn't support native grayscale format.
    for (int y=0; y<28; y++){
        for (int x=0; x<28; x++){
            uint8_t pixel=255.0*sample.imageData(0,28*y+x);
            image.setPixel(x, y, qRgb(pixel,pixel,pixel));
        }
    }
    ui->previewLabel->setPixmap(QPixmap::fromImage(image));

    ui->valueLabel->setText("Value: "+QString::number(sample.labels[0]));

    try {
        int cat=network->recognize(sample.imageData)[0];
        ui->recognizedValue->setText("Rec: "+QString::number(cat));
    } catch (...){
        ui->recognizedValue->setText("Rec: --");
    }
}

void MainWindow::resetState(){
    network->initState();
    showNetworkData();
}

void MainWindow::on_sampleChooser_valueChanged(int arg1)
{
    showPreview(arg1);
}


void MainWindow::onStateInfo(std::vector<QImage> img, int round, float error){
    weights=img;
    showWeights();
    ui->roundLabel->setText(QString::number(round));
    ui->errorLabel->setText(QString::number(error));
}

void MainWindow::startTrain(){
    ui->trainStartMenu->setText("Stop");
    ui->learningRateSelector->setPalette(paletteDefault);

    if (tester->isRunning())
        tester->stop();

    trainer->train(1,ui->learningRateSelector->value(),RENDER_CNT_PER_ROW,28,5);
}

void MainWindow::stopTrain(){
    trainer->stop();
    trainer->wait();
    ui->trainStartMenu->setText("Start");
}

void MainWindow::pushTrainState(){
    oldTrainState=trainer->isRunning();
    if (oldTrainState){
        stopTrain();
    }
}

void MainWindow::popTrainState(){
    if (oldTrainState){
        startTrain();
    }
}

void MainWindow::setLabelImage(QLabel *label, QImage &image){
    label->resize(image.size());
    label->setPixmap(QPixmap::fromImage(image));
}


void MainWindow::on_createFromDbMenu_triggered()
{
    pushTrainState();

    QImage img=RBMVisualizer::dream(*network,trainDb,ui->dreamDistanceSelector->value());
    setLabelImage(ui->dreamsLabel, img);

    popTrainState();
}

void MainWindow::on_dreamRandomMenu_triggered()
{
    pushTrainState();

    QImage img=RBMVisualizer::dream(*network,ui->dreamDistanceSelector->value());
    setLabelImage(ui->dreamsLabel, img);

    popTrainState();
}

void MainWindow::showNetworkData(){
    weights=network->plotWeights(RENDER_CNT_PER_ROW,28);
    showWeights();
}

void MainWindow::on_saveMenu_triggered()
{
    QString filename = QFileDialog::getSaveFileName(
            this,
            tr("Save network"),
            QDir::homePath(),
            tr("RBM file (*.net)") );

    if( filename.isNull() ){
        return;
    }


    pushTrainState();

    try{
        network->save(filename.toUtf8().constData());
    } catch (const char *str){
        std::cout<<"Save error: "<<str<<std::endl;
    }

    popTrainState();
}

void MainWindow::on_loadMenu_triggered()
{
    QString filename = QFileDialog::getOpenFileName(
            this,
            tr("Open network"),
            QDir::homePath(),
            tr("RBM files (*.net);;All files (*.*)") );

    if( filename.isNull() ){
        return;
    }



    pushTrainState();

    network->load(filename.toUtf8().constData());
    showNetworkData();

    popTrainState();
}

void MainWindow::on_trainStartMenu_triggered()
{
    if (trainer->isRunning()){
        stopTrain();
    } else {
        startTrain();
    }
}

void MainWindow::on_resetStateMenu_triggered()
{
    resetState();
}

void MainWindow::on_dreamBackMenu_triggered()
{
    pushTrainState();

    QImage img=RBMVisualizer::dream(*network,ui->dreamDistanceSelector->value(),10,28,2,true);
    setLabelImage(ui->dreamsLabel, img);

    popTrainState();
}

void MainWindow::on_layerSelector_valueChanged(int arg1)
{
    setLabelImage(ui->weightLabel, weights[arg1]);
}

void MainWindow::on_learningRateSelector_valueChanged(float arg1)
{
    if (trainer->isRunning()){
        QColor myColor( 255,0,0,255 );
        QPalette myPalette( ui->learningRateSelector->palette() );
        myPalette.setColor(QPalette::Active, QPalette::Base, myColor);
        ui->learningRateSelector->setPalette(myPalette);
    }
}

void MainWindow::on_dreamCategories_triggered()
{
    pushTrainState();

    QImage img=RBMVisualizer::dreamFromCategories(*network,ui->dreamDistanceSelector->value());
    setLabelImage(ui->dreamsLabel, img);

    popTrainState();
}

void MainWindow::onTrainInfo(int progress, float accuracy){
    ui->accuracyLabel->setText(QString::number(100.0*accuracy,'f',2)+" %");
    ui->testProgress->setValue(progress);
}

void MainWindow::on_testStartMenu_triggered()
{
    pushTrainState();

    tester->test();
}

void MainWindow::getImageBounds(const QImage &image, int &minX, int &maxX, int &minY, int &maxY){
    maxX=0;
    maxY=0;
    minX=image.width()-1;
    minY=image.height()-1;

    //not an effcient, but simple implementation.
    for (int x=0; x<image.width(); x++){
        for (int y=0; y<image.height(); y++){
            if ((image.pixel(x,y) & 0xFF)>50){
                if (x<minX)
                    minX=x;
                if (x>maxX)
                    maxX=x;
                if (y<minY)
                    minY=y;
                if (y>maxY)
                    maxY=y;
            }
        }
    }

    if (minX>maxX){
        minX=0;
        maxX=image.width();
    }

    if (minY>maxY){
        minY=0;
        maxY=image.height();
    }
}

QImage MainWindow::getActiveArea(const QImage &src){
    int minX,maxX,minY,maxY;
    getImageBounds(src, minX,maxX,minY,maxY);

    return src.copy(minX,minY,maxX-minX+1, maxY-minY+1);
}

void MainWindow::centroid(const QImage &image, int &centerX, int &centerY){
    float accuX=0;
    float accuY=0;

    int nOn=0;

    for (int x=0; x<image.width(); x++){
        for (int y=0; y<image.height(); y++){
            if ((image.pixel(x,y) & 0xFF)>50){
                accuX+=x;
                accuY+=y;
                nOn++;
            }
        }
    }

    centerX=(int)(accuX/nOn);
    centerY=(int)(accuY/nOn);
}

QImage MainWindow::minstPreprocess(const QImage &image){
    QImage img=getActiveArea(image).scaled(20,20,Qt::KeepAspectRatio, Qt::SmoothTransformation);

    int centerX, centerY;
    centroid(img, centerX, centerY);

    QImage result(28,28,QImage::Format_RGB32);
    result.fill(qRgb(0,0,0));

    int x=13-centerX;
    int y=13-centerY;

    if (x<0)
        x=0;
    if (y<0)
        y=0;

    if (x+img.width() > result.width())
        x=result.width() - img.width();

    if (y+img.height() > result.height())
        y=result.height() - img.height();

    QPainter painter(&result);
    painter.drawImage(x,y,img);
    painter.end();

    return result;
}

void MainWindow::onImageDrawn(QImage image){
    pushTrainState();

    QImage img=minstPreprocess(image);
    Matrix data(1,28*28);

    setLabelImage(ui->drawinImage, img);

    for (int r=0; r<28; r++){
        for (int c=0; c<28; c++){
            data(1,r*28+c)=((float)(img.pixel(r,c) & 0xFF))/255.0;
        }
    }

    int rec=network->recognize(data)[0];

    ui->drawRecognitionLabel->setText(QString::number(rec));

    popTrainState();
}

void MainWindow::on_resetImageBtn_clicked()
{
    ui->paintLabel->clear();
}
