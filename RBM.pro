#-------------------------------------------------
#
# Project created by QtCreator 2015-08-28T13:05:59
#
#-------------------------------------------------
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RBM
TEMPLATE = app

CONFIG += c++11

INCLUDEPATH += Matrix Network Database UI

QMAKE_CXXFLAGS_RELEASE *= -O3

macx {
    DEFINES += USE_CBLAS=1 OSX=1
    LIBS += -framework Accelerate
}

SOURCES += main.cpp\
    Matrix/Matrix.cpp \
    Matrix/MatrixPayload.cpp \
    Matrix/MatrixPool.cpp \
    Network/DBN.cpp \
    Network/RBM.cpp \
    Network/RBMState.cpp \
    Database/MNIST.cpp \
    Network/Common.cpp \
    Network/TrainThread.cpp \
    UI/mainwindow.cpp \
    UI/RBMVisualizer.cpp \
    Network/GenerativeNetwork.cpp \
    Network/TestThread.cpp \
    UI/PaintableImage.cpp

HEADERS  += \
    Matrix/Matrix.h \
    Matrix/MatrixPayload.h \
    Matrix/MatrixPool.h \
    Network/DBN.h \
    Network/GenerativeNetwork.h \
    Network/RBM.h \
    Network/RBMState.h \
    Database/Database.h \
    Database/MNIST.h \
    Network/Common.h \
    Network/TrainThread.h \
    UI/mainwindow.h \
    UI/RBMVisualizer.h \
    Network/TestThread.h \
    UI/PaintableImage.h

FORMS += UI/mainwindow.ui

RESOURCES += \
    resources.qrc
