//
//  Matrix.cpp
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 03..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//
#include "Matrix.h"
#include "MatrixPool.h"
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <random>
#include <iostream>

#ifdef OSX
    #include <Accelerate/Accelerate.h>
#endif


//Asserts on dimension mismatch
#define DIMCHECK(a,b) assert(((a)->rows()==(b)->rows()) && ((a)->cols()==(b)->cols()))

//Defines a new matrix with the same size as the current
#define DEFINE_SAME_SIZE_MATRIX(name) Matrix name(rows(), cols())
#define DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(name) Matrix name(payload->nRows, payload->nCols)


Matrix::IndexProxy::IndexProxy(float *data, int stride){
    this->data=data;
    this->stride=stride;
}

Matrix::IndexProxy::IndexProxy(const IndexProxy &other){
    data=other.data;
    stride=other.stride;
}

float & Matrix::IndexProxy::operator[] (const int nIndex) const{
    return data[stride*nIndex];
}

Matrix::Matrix(){
    payload=NULL;
}

Matrix::Matrix(int nRows, int nCols){
    //Allocate a new payload.
    payload=MatrixPool::allocate(nRows, nCols);
    isTranspose=false;
}

Matrix Matrix::RowVector(int cols){
    return Matrix(1, cols);
}

Matrix Matrix::ColVector(int rows){
    return Matrix(rows,1);
}


Matrix::Matrix(const Matrix &other){
    this->payload=NULL;
    (*this)=other;
}

Matrix Matrix::horizontalMerge(const Matrix &left, const Matrix &right){
    assert(left.rows() == right.rows());

    int nR=left.rows();
    int nC_left=left.cols();
    int nC_right=right.cols();

    Matrix result(nR, nC_left+nC_right);

    for (int r=0; r<nR; r++){
        IndexProxy leftRow=left.row(r);
        IndexProxy rightRow=right.row(r);
        IndexProxy resultRow=result.row(r);

        for (int c=0; c<nC_left; c++){
            resultRow[c]=leftRow[c];
        }

        for (int c=0; c<nC_right; c++){
            resultRow[nC_left+c]=rightRow[c];
        }
    }

    return result;
}

void Matrix::releasePayload(){
    if (!payload)
        return;
    
    //If deallocated, handle the reference counter. If needed,
    //push the payload back in the pool
    payload->referenceCounter--;
    if (payload->referenceCounter==0){
        MatrixPool::deallocate(payload);
    }
}

Matrix::~Matrix(){
    releasePayload();
}

Matrix::IndexProxy Matrix::operator[] (const int nIndex) const{
    if (isTranspose)
        return Matrix::IndexProxy(&payload->data[nIndex], payload->nCols);
    else
        return Matrix::IndexProxy(&payload->data[nIndex*payload->nCols],1);
}

Matrix::IndexProxy Matrix::row(int index) const{
    return (*this)[index];
}

Matrix::IndexProxy Matrix::col(int index) const{
    if (isTranspose)
        return Matrix::IndexProxy(&payload->data[index*payload->nCols],1);
    else
        return Matrix::IndexProxy(&payload->data[index], payload->nCols);
}

Matrix & Matrix::operator=(const Matrix &other){
    //First delete the old payload.
    releasePayload();
    
    //When copying, increase the reference counter.
    this->payload=other.payload;
    payload->referenceCounter++;
    
    this->isTranspose=other.isTranspose;
    
    return *this;
}

Matrix Matrix::Zeros(int nRows, int nCols){
    //Construct a zero matrix.
    Matrix r(nRows, nCols);
    
    const int n=nRows*nCols;
    for (int a=0; a<n; a++){
        r.payload->data[a]=0.0;
    }
    
    return r;
}

Matrix Matrix::Ones(int nRows, int nCols){
    //Construct an all-one matrix
    Matrix r(nRows, nCols);
    
    const int n=nRows*nCols;
    for (int a=0; a<n; a++){
        r.payload->data[a]=1.0;
    }
    
    return r;
}

Matrix Matrix::UniformRandom(int nRows, int nCols, float min, float max){
    //Construct uniform random matrix
    Matrix r(nRows, nCols);
    
    const int n=nRows*nCols;
    for (int a=0; a<n; a++){
        r.payload->data[a]=min + (((float)rand())/((float)RAND_MAX))*(max-min);
    }
    
    return r;
}

Matrix Matrix::NormalRandom(int nRows, int nCols, float mu, float sigma){
    Matrix r(nRows, nCols);
    
    std::default_random_engine generator;
    generator.seed((int)time(0));
    std::normal_distribution<float> distribution(mu,sigma);
    
    const int n=nRows*nCols;
    for (int a=0; a<n; a++){
        r.payload->data[a]=distribution(generator);
    }
    
    return r;
}

Matrix& Matrix::operator+=(const Matrix& rhs){
    DIMCHECK(this, &rhs);
    
    int nRow=rows();
    int nCol=cols();
    
    //We cannot just add payload element by element,
    //because each of matrices can be transpose of other.
    for (int r=0; r<nRow; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=rhs.row(r);
        
        for (int c=0; c<nCol; c++){
            myRow[c]+=otherRow[c];
        }
    }
    
    return *this;
}

Matrix Matrix::operator+(const Matrix& rhs) const{
    DIMCHECK(this, &rhs);
    
    DEFINE_SAME_SIZE_MATRIX(result);
    
    int nRow=rows();
    int nCol=cols();
    
    for (int r=0; r<nRow; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=rhs.row(r);
        IndexProxy resultRow=result.row(r);
        
        for (int c=0; c<nCol; c++){
            resultRow[c]=myRow[c]+otherRow[c];
        }
    }
    
    return result;
}

Matrix& Matrix::operator+=(float rhs){
    const int n=payload->nRows*payload->nCols;
    for (int a=0; a<n; a++){
        payload->data[a]+=rhs;
    }
    
    return *this;
}

Matrix Matrix::operator+(float rhs) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        result.payload->data[a]=payload->data[a]+rhs;
    }
    
    return result;
}

Matrix operator+(float lhs, const Matrix& rhs){
    return rhs+lhs;
}




Matrix& Matrix::operator-=(const Matrix& rhs){
    DIMCHECK(this, &rhs);
    
    int nRow=rows();
    int nCol=cols();
    
    //We cannot just add payload element by element,
    //because each of matrices can be transpose of other.
    for (int r=0; r<nRow; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=rhs.row(r);
        
        for (int c=0; c<nCol; c++){
            myRow[c]-=otherRow[c];
        }
    }
    
    return *this;
}

Matrix Matrix::operator-(const Matrix& rhs) const{
    DIMCHECK(this, &rhs);
    
    DEFINE_SAME_SIZE_MATRIX(result);
    
    int nRow=rows();
    int nCol=cols();
    
    for (int r=0; r<nRow; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=rhs.row(r);
        IndexProxy resultRow=result.row(r);
        
        for (int c=0; c<nCol; c++){
            resultRow[c]=myRow[c]-otherRow[c];
        }
    }
    
    return result;
}

Matrix& Matrix::operator-=(float rhs){
    const int n=payload->nRows*payload->nCols;
    for (int a=0; a<n; a++){
        payload->data[a]-=rhs;
    }
    
    return *this;
}

Matrix Matrix::operator-(float rhs) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        result.payload->data[a]=payload->data[a]-rhs;
    }
    
    return result;
}

Matrix operator-(float lhs, const Matrix& rhs){
    return rhs-lhs;
}


int Matrix::rows() const{
    if (isTranspose)
        return payload->nCols;
    else
        return payload->nRows;
}

int Matrix::cols() const{
    if (isTranspose)
        return payload->nRows;
    else
        return payload->nCols;
}

Matrix Matrix::transpose() const{
    Matrix result(*this);
    result.isTranspose = !result.isTranspose;
    return result;
}

float &Matrix::operator ()(int r, int c){
    if (isTranspose){
        return payload->data[payload->nCols * c + r];
    } else {
        return payload->data[payload->nCols * r + c];
    }
}

const float &Matrix::operator ()(int r, int c) const{
    if (isTranspose){
        return payload->data[payload->nCols * c + r];
    } else {
        return payload->data[payload->nCols * r + c];
    }
}

Matrix Matrix::operator* (const Matrix &other) const{
    assert(cols()==other.rows());
#ifdef USE_CBLAS
    return cblasMult(other);
#else
    
    int nR=rows();
    int nC=other.cols();
    Matrix result(nR, nC);
    
    int sumWidth=cols();
    
    for (int r=0; r<nR; r++){
        IndexProxy leftRow=row(r);
        for (int c=0; c<nC; c++){
            IndexProxy rightCol=other.col(c);
            
            float accu=0;
            for (int i=0; i<sumWidth; i++){
                accu+=leftRow[i]*rightCol[i];
            }
            
            result(r,c)=accu;
        }
    }
    
    return result;
#endif
}

Matrix &Matrix::operator*= (const Matrix &other){
    *this=(*this)*other;
    return *this;
}

Matrix Matrix::operator* (const float other) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        result.payload->data[a]=payload->data[a]*other;
    }
    
    return result;
}

Matrix &Matrix::operator*= (const float other){
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        payload->data[a]*=other;
    }
    
    return *this;
}

Matrix operator*(float lhs, const Matrix& rhs){
    return rhs*lhs;
}

Matrix Matrix::operator/ (const float other) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        result.payload->data[a]=payload->data[a]/other;
    }
    
    return result;
}

Matrix &Matrix::operator/= (const float other){
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        payload->data[a]/=other;
    }
    
    return *this;
}

Matrix Matrix::rowwiseAdd(const Matrix &other, bool inPlace) const{
    assert(other.rows()==1 && cols()==other.cols());
    
    Matrix result;
    if (inPlace)
        result = *this;
    else
        result=DEFINE_SAME_SIZE_MATRIX();

    
    int nR=rows();
    int nC=cols();
    
    for (int r=0; r<nR; r++){
        IndexProxy a=row(r);
        IndexProxy b=other.row(0);
        IndexProxy res=result.row(r);
        
        for (int c=0; c<nC; c++){
            res[c]=a[c]+b[c];
        }
    }
    
    return result;
}

Matrix Matrix::colwiseAdd(const Matrix &other, bool inPlace) const{
    assert(other.cols()==1 && rows()==other.rows());
    
    Matrix result;
    if (inPlace)
        result = *this;
    else
        result=DEFINE_SAME_SIZE_MATRIX();

    
    int nR=rows();
    int nC=cols();
    
    for (int c=0; c<nC; c++){
        IndexProxy a=col(c);
        IndexProxy b=other.col(0);
        IndexProxy res=result.col(c);
        
        for (int r=0; r<nR; r++){
            res[r]=a[r]+b[r];
        }
    }
    
    return result;
    
}

Matrix Matrix::rowwiseSubtract(const Matrix &other, bool inPlace) const{
    assert(other.rows()==1 && cols()==other.cols());
    
    Matrix result;
    if (inPlace)
        result = *this;
    else
        result=DEFINE_SAME_SIZE_MATRIX();
    
    int nR=rows();
    int nC=cols();
    
    for (int r=0; r<nR; r++){
        IndexProxy a=row(r);
        IndexProxy b=other.row(0);
        IndexProxy res=result.row(r);
        
        for (int c=0; c<nC; c++){
            res[c]=a[c]-b[c];
        }
    }
    
    return result;
}

Matrix Matrix::colwiseSubtract(const Matrix &other, bool inPlace) const{
    assert(other.cols()==1 && rows()==other.rows());
    
    Matrix result;
    if (inPlace)
        result = *this;
    else
        result=DEFINE_SAME_SIZE_MATRIX();

    
    int nR=rows();
    int nC=cols();
    
    for (int c=0; c<nC; c++){
        IndexProxy a=col(c);
        IndexProxy b=other.col(0);
        IndexProxy res=result.col(c);
        
        for (int r=0; r<nR; r++){
            res[r]=a[r]-b[r];
        }
    }
    
    return result;
}

Matrix Matrix::sumOfRows() const{
    int nR=rows();
    int nC=cols();
    
    Matrix result=Matrix::Zeros(1, nC);
    
    for (int r=0; r<nR; r++){
        IndexProxy thisrow=row(r);
        for (int c=0; c<nC; c++){
            result.payload->data[c] += thisrow[c];
        }
    }
    
    return result;
}

Matrix Matrix::sumOfCols() const{
    int nR=rows();
    int nC=cols();
    
    Matrix result=Matrix::Zeros(nR, 1);
    
    for (int c=0; c<nC; c++){
        IndexProxy thiscol=col(c);
        for (int r=0; r<nR; r++){
            result.payload->data[r] += thiscol[r];
        }
    }
    
    return result;
}


void Matrix::save(std::ofstream &file) const{
    int nR=rows();
    int nC=cols();
    
    file.write((char*)&nR, sizeof(nR));
    file.write((char*)&nC, sizeof(nC));
    
    for (int r=0; r<nR; r++){
        IndexProxy thisrow=row(r);
        
        for (int c=0; c<nC; c++){
            float w=thisrow[c];
            file.write((char*)&w, sizeof(w));
        }
    }
}

void Matrix::load(std::ifstream &file){
    int nR;
    int nC;
    
    file.read((char*)&nR, sizeof(nR));
    file.read((char*)&nC, sizeof(nC));
    
    if (payload==NULL || nR!=rows() || nC!=cols()){
        releasePayload();
        payload=MatrixPool::allocate(nR, nC);
    }
    
    for (int r=0; r<nR; r++){
        IndexProxy thisrow=row(r);
        
        for (int c=0; c<nC; c++){
            float w;
            file.read((char*)&w, sizeof(w));
            thisrow[c]=w;
        }
    }
}

Matrix Matrix::elementwise(float (*transform)(float d)) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nRows*payload->nCols;
    for (int a=0; a<n; a++){
        result.payload->data[a]=transform(payload->data[a]);
    }
    return result;
}

Matrix Matrix::meanOfCols() const{
    return sumOfCols()/cols();
}

Matrix Matrix::meanOfRows() const{
    return sumOfRows()/rows();
}

float Matrix::sum() const{
    float accu=0;
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        accu+=payload->data[a];
    }
    
    return accu;
}

void Matrix::minMax(float &min, float &max) const{
    min=max=payload->data[0];
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        float d=payload->data[a];
        if (d<min)
            min=d;
        else if (d>max)
            max=d;
    }
}

float Matrix::mean() const{
    return sum()/(payload->nCols*payload->nRows);
}

void  Matrix::minMaxCol(int col, float &min, float &max) const{
    IndexProxy c=this->col(col);
    int nRows=rows();

    min=c[0];
    max=c[0];

    for (int row=0; row<nRows; row++){
        float curr=c[row];

        if (curr<min)
            min=curr;
        else if (curr>max)
            max=curr;
    }
}
void  Matrix::minMaxRow(int row, float &min, float &max) const{
    IndexProxy r=this->row(row);
    int nCols=cols();

    min=r[0];
    max=r[0];

    for (int col=0; col<nCols; col++){
        float curr=r[col];

        if (curr<min)
            min=curr;
        else if (curr>max)
            max=curr;
    }
}

Matrix Matrix::clone() const{
    int nR=rows();
    int nC=cols();

    Matrix res(nR,nC);
    for (int r=0; r<nR; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=res.row(r);

        for (int c=0; c<nC; c++){
            otherRow[c]=myRow[c];
        }
    }

    return res;
}

Matrix Matrix::makeRowContinous() const{
    if (isTranspose){
        return clone();
    } else {
        return *this;
    }
}

Matrix Matrix::makeColContinous() const{
     if (isTranspose){
         return *this;
     } else {
        Matrix res=transpose().clone();
        res.isTranspose=true;

        return res;
     }
}

Matrix Matrix::submatrix(int startRow, int startCol, int nRow, int nCol){
    Matrix result(nRow,nCol);

    for (int r=0; r<nRow; r++){
        IndexProxy inputRow=row(r+startRow);
        IndexProxy outputRow=result.row(r);

        for (int c=0; c<nCol; c++){
            outputRow[c]=inputRow[startCol + c];
        }
    }

    return result;
}

#ifdef USE_CBLAS
Matrix Matrix::cblasMult (const Matrix &other) const{
    CBLAS_TRANSPOSE trA=isTranspose ? CblasTrans : CblasNoTrans;
    CBLAS_TRANSPOSE trB=other.isTranspose ? CblasTrans : CblasNoTrans;

    int M=this->rows();
    int N=other.cols();
    int K=this->cols();

    Matrix result(M, N);

    //printf("M: %d, N: %d, K: %d\r\n", M, N, K);
    //printf("A: %c, B: %c\r\n", isTranspose ? 'T' : '_', other.isTranspose ? 'T' : '_');

    cblas_sgemm(CblasRowMajor, trA, trB, M, N, K, 1.0f, payload->data, this->payload->nCols, other.payload->data, other.payload->nCols, 0.0f, result.payload->data, N);

    return result;
}

#endif

