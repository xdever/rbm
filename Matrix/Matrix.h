//
//  Matrix.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 03..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef __RBM__Matrix__
#define __RBM__Matrix__

#include <stdio.h>
#include "MatrixPayload.h"
#include <fstream>

// The matrix class can be passed cheaply from function to fuction, because
// it's payload only copies when the operation is requires so.
//
// Transposing a matrix doesn't copy any data.
//
// The Matrix() constructor doesn't creates the payload. It is designed for
// uses like Matrix a; if (b) a=c; else a=d; Only the destructor and the
// operator = are valid operations.
//
class Matrix{
    private:
        MatrixPayload *payload;
        bool isTranspose;
    
        void releasePayload();

        #ifdef USE_CBLAS
        Matrix cblasMult (const Matrix &other) const;
        #endif
    public:
        // IndexProxy is used to manage matrix indexing. It works both with
        // normal and transposed matrices.
        //
        // Matrix class uses this internally to. It is not woth of creating
        // an incrementing accumulator to move the index from row to row (or
        // col to col), because both multiplication and addition is equally
        // fast on Intel CPUs.
        class IndexProxy{
            private:
                float *data;
                int stride;
            public:
                IndexProxy(const IndexProxy &other);
                IndexProxy(float *data, int stride);
                float & operator[] (const int nIndex) const;
        };
    
        Matrix();
        Matrix(int nRows, int nCols);
        Matrix(const Matrix &other);
        ~Matrix();
    
        static Matrix Zeros(int nRows, int nCols);
        static Matrix Ones(int nRows, int nCols);
        static Matrix UniformRandom(int nRows, int nCols, float min, float max);
        static Matrix NormalRandom(int nRows, int nCols, float mu, float sigma);
        static Matrix RowVector(int cols);
        static Matrix ColVector(int rows);
        static Matrix horizontalMerge(const Matrix &left, const Matrix &right);
    
        Matrix & operator=(const Matrix &other);
    
        Matrix& operator+=(const Matrix& rhs);
        Matrix& operator+=(float rhs);
        Matrix operator+(const Matrix& rhs) const;
        Matrix operator+(float rhs) const;
        friend Matrix operator+(float lhs, const Matrix& rhs);
    
        Matrix& operator-=(const Matrix& rhs);
        Matrix& operator-=(float rhs);
        Matrix operator-(const Matrix& rhs) const;
        Matrix operator-(float rhs) const;
        friend Matrix operator-(float lhs, const Matrix& rhs);
    
        IndexProxy operator[] (const int nIndex) const;
        IndexProxy row(int index) const;
        IndexProxy col(int index) const;
    
        Matrix operator* (const Matrix &other) const;
        Matrix &operator*= (const Matrix &other);
        Matrix operator* (const float other) const;
        Matrix &operator*= (const float other);


    
        Matrix operator/ (const float other) const;
        Matrix &operator/= (const float other);
        friend Matrix operator*(float lhs, const Matrix& rhs);
    
        //Faster than [][]
        float &operator ()(int r, int c);
        const float &operator ()(int r, int c) const;
    
        Matrix transpose() const;
    
        int rows() const;
        int cols() const;
    
        Matrix rowwiseAdd(const Matrix &other, bool inPlace=false) const;
        Matrix colwiseAdd(const Matrix &other, bool inPlace=false) const;
    
        Matrix rowwiseSubtract(const Matrix &other, bool inPlace=false) const;
        Matrix colwiseSubtract(const Matrix &other, bool inPlace=false) const;
    
        Matrix sumOfCols() const;
        Matrix sumOfRows() const;
    
        Matrix meanOfCols() const;
        Matrix meanOfRows() const;
    
        void save(std::ofstream &file) const;
        void load(std::ifstream &file);
    
        Matrix elementwise(float (*transform)(float d)) const;
    
        float sum() const;
        float mean() const;
    
        void minMax(float &min, float &max) const;
        void minMaxCol(int col, float &min, float &max) const;
        void minMaxRow(int row, float &min, float &max) const;

        Matrix makeRowContinous() const;
        Matrix makeColContinous() const;
        Matrix clone() const;

        Matrix submatrix(int startRow, int startCol, int nRow, int nCol);
    
};

#endif /* defined(__RBM__Matrix__) */
