//
//  MatrixPool.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 03..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef __RBM__MatrixPool__
#define __RBM__MatrixPool__

#include <stdio.h>
#include "MatrixPayload.h"
#include <list>



/*
 * Class for pooling matrix allocations. This will speed up
 * cascaded matrix operations drastically.
 *
 */

class MatrixPool{
    private:
        static std::list<MatrixPayload *> deallocatedMatrices;
    public:
        static int historyLength;
    
        static MatrixPayload *allocate(int nRows, int nCols);
        static void deallocate(MatrixPayload *data);
        static void cleanup();
};

#endif /* defined(__RBM__MatrixPool__) */
