//
//  MatrixPayload.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 03..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef __RBM__MatrixPayload__
#define __RBM__MatrixPayload__

#include <stdio.h>



struct MatrixPayload{
    //Raw payload. Moved to a separate
    //structure to be able to copy it with one
    //memory operation. It is also pooled.
    
    float *data;
    int referenceCounter;
    int nRows;
    int nCols;

    MatrixPayload(int nRows, int nCols);
    ~MatrixPayload();
};

#endif /* defined(__RBM__MatrixPayload__) */
