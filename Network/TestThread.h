#ifndef TRAINTHREAD_H
#define TRAINTHREAD_H

#include <QThread>
#include "Database.h"
#include "GenerativeNetwork.h"

/**
 * @brief Thread for running test on a given database.
 *
 * Every element of the database is sent to the recognizer and it's
 * result is checked.
 *
 */
class TestThread:public QThread
{
    Q_OBJECT
public:
    TestThread(GenerativeNetwork *network, Database *db);
    ~TestThread();

    void stop();
    bool isRunning();

    void test();
signals:
    void stateInfo(int, float);
private:
    GenerativeNetwork *network;
    Database *db;

    unsigned int current;
    unsigned int correct;

    bool running=true;
    void run();
};

#endif // TRAINTHREAD_H
