#ifndef DBN_H
#define DBN_H

#include <RBM.h>
#include <vector>
#include <GenerativeNetwork.h>
#include "Matrix.h"

class DBN;

class DBNSampler: public GenerativeNetworkSampler{
    private:
        friend class DBN;

        const DBN *owner;
        std::shared_ptr<GenerativeNetworkSampler> state;
    public:
        DBNSampler(const DBN *owner);
        DBNSampler(const DBNSampler &other);

        virtual ~DBNSampler();
        virtual Matrix sample(int nSteps);

        int layerSize() const;
};

class DBN: public GenerativeNetwork
{
private:
    friend class RBMVisualizer;
    friend class DBNSampler;

    RBM ** layers;
    int layerCnt;

    int trainSteps;

    int classificationCategories;

    void clearLayers();

    Matrix upPropagate(const Matrix &data, int stopAt=-1, Matrix const * labels=NULL) const;
    Matrix downPropagate(const Matrix &data, int layerOffset=0, Matrix * labels=NULL) const;

    Matrix createLabelMatrix(const std::vector<int> &labels);
public:
    DBN(int nLayers, int nCategories, int nVisible, int nLayer1, ...);
    virtual ~DBN();

    virtual int getVisibleSize() const;
    virtual int getHiddenSize(int layer=-1) const;

    virtual float trainStep(const Database &db, int batchSize=10, float learningRate=0.1, int cd=1);

    void initState();

    void save(const std::string filename) const;
    void load(const std::string filename);

    std::vector<QImage> plotWeights(int nInRow, int weightImageW) const;

    int getLayerCount() const;

    Matrix visualizeSample(const Matrix &data) const;

     std::shared_ptr<GenerativeNetworkSampler> startSampling(const Matrix &data, bool startWithHidden=false) const;
     std::shared_ptr<GenerativeNetworkSampler> startCategorySampling(const std::vector<int> &categoryList) const;

     int getCategoryCount() const;

     std::vector<int> recognize(const Matrix &image) const;
};

#endif // DBN_H
