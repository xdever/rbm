#include "TestThread.h"
#include "RBMVisualizer.h"
#include <iostream>

TestThread::TestThread(GenerativeNetwork *network, Database *db)
{
    this->network=network;
    this->db=db;
    running=false;
}

void TestThread::test(){
    running=true;
    current=0;
    correct=0;

    start();
}

void TestThread::stop(){
    running=false;
}

TestThread::~TestThread()
{
    stop();
    wait();
}

bool TestThread::isRunning(){
    return running;
}

void TestThread::run(){
    const int batchSize=20;
    bool done=false;

    emit stateInfo(0, 100.0);

    while(running){
        //Get sample batch from the database and send it to recognizer.
        DatabaseSample data=db->getSequential(current, batchSize);
        std::vector<int> recognized=network->recognize(data.imageData);

        //Check recognition results.
        for (int a=0; a<batchSize; a++){
            if (data.labels[a]==recognized[a])
                correct++;
        }

        current+=batchSize;
        if (current>(db->count()-batchSize)){
            done=true;
            running=false;
        }

        //Send statistics.
        emit stateInfo(100.0*current/db->count(), ((float)correct)/current);
    }

    if (!done)
        emit stateInfo(100, ((float)correct)/current);
}

