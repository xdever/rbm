#include "DBN.h"
#include <stdarg.h>
#include "RBMVisualizer.h"
#include <iostream>
#include <fstream>

/**
 * @brief Create a deep belief network.
 *
 * @param nLayers number of layers in the network
 * @param nCategories number of classification categories. Use 0 if no classification is required.
 * @param nVisible number of visible units
 * @param nLayer1 number of hidden units in layer1
 * @param ... number of hidden units in other layers.
 *
 */
DBN::DBN(int nLayers, int nCategories, int nVisible, int nLayer1, ...)
{
    layerCnt = nLayers;

    layers = new RBM*[nLayers];
    layers[0]=new RBM(nVisible, nLayer1);

    int lastCnt=nLayer1;

    va_list vl;
    va_start(vl,nLayer1);

    for (int a=1; a<nLayers; a++){
        int val=va_arg(vl,int);

        //If classification categories are given, the input to the last layer
        //should be increased.
        if (a==nLayers-1){
            lastCnt+=nCategories;
        }

        layers[a]=new RBM(lastCnt, val);
        lastCnt=val;
    }

    va_end(vl);

    classificationCategories=nCategories;
}

/**
 * @brief Deallocates current layers of the network.
 *
 */
void DBN::clearLayers(){
    for (int a=0; a<layerCnt; a++){
        delete layers[a];
    }

    delete [] layers;
}

DBN::~DBN()
{
    clearLayers();
}

/**
 * @brief Create bias matrix for the category input.
 *
 * @param labels list of labels.
 * @return matrix, where every row has one column (corresponding to the label) set to 1 and all others to 0.
 *
 */
Matrix DBN::createLabelMatrix(const std::vector<int> &labels){
    int cnt=labels.size();
    Matrix result=Matrix::Zeros(cnt, classificationCategories);

    for (int a=0; a<cnt; a++){
        result(a, labels[a])=1.0;
    }

    return result;
}

/**
 * @brief Do a train step.
 *
 * @param db the database to get samples from.
 * @param batchSize the size of the batch.
 * @param learningRate learning rate.
 * @param cd the number of CD steps to take.
 * @return error.
 *
 */
float DBN::trainStep(const Database &db, int batchSize, float learningRate, int cd){
    //DatabaseSample data=db.getSampleFromAllCategories((batchSize+db.categoryCount()-1)/db.categoryCount());
    //printf("bs: %d\r\n", batchSize);
    DatabaseSample data=db.getSequential(rand() % (db.count()-batchSize), batchSize);

    Matrix hiddenState;

    float error;

    int l=(trainSteps/10000) % layerCnt;

    //std::cout<<"Layer: "<<l<<std::endl;

    Matrix mData;

    if (l!=0){
        mData=upPropagate(data.imageData, l-1);
    } else {
        mData=data.imageData;
    }

    if (classificationCategories!=0 && l==layerCnt-1){
        mData = Matrix::horizontalMerge(mData, createLabelMatrix(data.labels));
    }

    if (trainSteps % 100 == 0){
        float min, max;
        layers[l]->weightMinMax(min, max);
        printf("Weight: min: %f, max: %f\r\n", min, max);


        layers[l]->state.visibleBias.minMax(min, max);
        printf("Visible bias: min: %f, max: %f\r\n", min, max);

        layers[l]->state.hiddenBias.minMax(min, max);
        printf("Hidden bias: min: %f, max: %f\r\n\r\n\r\n", min, max);

        fflush(stdout);
    }

   // for (int a=0; a<l; a++)
     //   data=layers[a]->vis2hid(data);

    //for (int l=0; l<layerCnt; l++){
   //     if (trainSteps<(l*2000)){
    //        break;
    //    }

        float e=layers[l]->train(mData, cd, learningRate);
      //  data=layers[l]->vis2hid(data);

        //if (a==0)
            error=e;
   // }

    trainSteps++;


    return error;
}

/**
 * @brief Propagate data up the network.
 *
 * When propagating bethween last two layers, labels are
 * appended to the data coming from the second to last layer.
 * This makes possible for the network to learn classification
 * labels.
 *
 * Parameter "labels", if set, is set to the label data when prpagating
 *
 * @param data the data presented to the visible layer.
 * @param stopAt stop at this level. Set to -1 to propagate all the way up.
 * @param labels label dataset. If not given, 0.1s are presented to the label neurons.
 * @return the data at the "stopAt" label.
 */
Matrix DBN::upPropagate(const Matrix &data, int stopAt, Matrix const * labels) const{
    Matrix currData=data;

    if (stopAt<0)
        stopAt=layerCnt-1;

    for (int l=0; l<=stopAt; l++){
        //Merge labels to last up-propagation step. We should merge at
        //last layer.
        if (classificationCategories!=0 && l==layerCnt-1){
            if (labels)
                currData = Matrix::horizontalMerge(currData, *labels);
            else
                currData = Matrix::horizontalMerge(currData, 0.1*Matrix::Ones(currData.rows(), classificationCategories));
        }
        currData=layers[l]->vis2hid(currData);
    }

    return currData;
}

/**
 * @brief Propagate data down to the network.
 *
 * Parameter "labels", if set, is set to the down propagated label data.
 * If not set, the label data is simply thrown away.
 *
 * @param data the data presented to the hidden layer.
 * @param layerOffset layer offset, from backwards, to start from.
 * @param labels the destination label matrix. Set to NULL to ignore.
 * @return the data at visible layer.
 */
Matrix DBN::downPropagate(const Matrix &data, int layerOffset, Matrix * labels) const{
    Matrix currData=data;

    for (int l=layerCnt-1-layerOffset; l>=0; l--){
        //Cut layers off from the first down propagation step. We should cut at
        //second to last layer.
        if (classificationCategories!=0 && l==layerCnt-2){
            if (labels!=NULL){
                *labels = currData.submatrix(0, currData.cols()-classificationCategories, currData.rows(), classificationCategories);
            }

            currData=currData.submatrix(0, 0, currData.rows(), currData.cols()-classificationCategories);
        }
        currData=layers[l]->hid2vis(currData);
    }

    return currData;
}

/**
 * @brief Start sampling the network from a given data.
 *
 * @param data data to initialize the network with.
 * @param startWithHidden if true, data is presented to the last hidden layer, if not, it is presented to the visible units.
 * @return sampler object.
 */
std::shared_ptr<GenerativeNetworkSampler> DBN::startSampling(const Matrix &data, bool startWithHidden) const{
    Matrix d;
    if (startWithHidden){
        d=data;
    } else {
        d=upPropagate(data);
    }

    DBNSampler *sampler=new DBNSampler(this);
    sampler->state=layers[layerCnt-1]->startSampling(d,true);

    return std::shared_ptr<GenerativeNetworkSampler>(sampler);
}



int DBN::getVisibleSize() const{
    return layers[0]->getVisibleSize();
}

int DBN::getHiddenSize(int layer) const{
    if (layer<0 || layer>=layerCnt)
        layer=layerCnt-1;

    return layers[layer]->getHiddenSize();
}

void DBN::initState(){
    for (int l=0; l<layerCnt; l++){
        layers[l]->initState();
    }

    trainSteps=0;
}



void DBN::save(const std::string filename) const{
    std::ofstream file(filename);

    if (!file.is_open()){
        throw "Failed to open file.";
    }

    file.write((char*)&layerCnt,sizeof(layerCnt));
    file.write((char*)&classificationCategories,sizeof(classificationCategories));
    file.write((char*)&trainSteps,sizeof(trainSteps));

    for (int l=0; l<layerCnt; l++){
        layers[l]->state.save(file);
    }
}

void DBN::load(const std::string filename){
    std::ifstream file(filename);

    if (!file.is_open()){
        throw "Failed to open file.";
    }

    clearLayers();
    file.read((char*)&layerCnt,sizeof(layerCnt));
    file.read((char*)&classificationCategories,sizeof(classificationCategories));
    file.read((char*)&trainSteps,sizeof(trainSteps));

    layers = new RBM*[layerCnt];

    for (int l=0; l<layerCnt; l++){
        RBMState state(file);
        layers[l]=new RBM(0,0, &state);
    }
}

std::vector<QImage> DBN::plotWeights(int nInRow, int weightImageW) const{
    return RBMVisualizer::plotWeights(*this, nInRow, weightImageW);
}

int DBN::getLayerCount() const{
    return layerCnt;
}

DBNSampler::DBNSampler(const DBN *owner){
    this->owner=owner;
}

DBNSampler::DBNSampler(const DBNSampler &other){
    this->owner=other.owner;
    this->state=other.state;
}

DBNSampler::~DBNSampler(){

}

Matrix DBNSampler::sample(int nSteps){
    return owner->downPropagate(state->sample(nSteps),1);
}

int DBNSampler::layerSize() const{
    return owner->getVisibleSize();
}

/**
 * @brief Start sampling the network in a way that generates a sample from a given categry list.
 *
 * It initializes the input of the last hidden layer based on the given category list. The activity of
 * regular input neurons are set to small random values.
 *
 * @param categoryList the list of categories to generate from.
 * @return sampler object.
 */
std::shared_ptr<GenerativeNetworkSampler> DBN::startCategorySampling(const std::vector<int> &categoryList) const{
    int lastVisibleCnt=layers[layerCnt-1]->getVisibleSize();

    Matrix d=Matrix::UniformRandom(categoryList.size(),lastVisibleCnt,0.0,0.1);
    for (size_t i=0; i<categoryList.size(); i++){
        d(i, lastVisibleCnt-classificationCategories+categoryList[i])=1000000.0;
    }

    DBNSampler *sampler=new DBNSampler(this);
    sampler->state=layers[layerCnt-1]->startSampling(d,false);

    return std::shared_ptr<GenerativeNetworkSampler>(sampler);
}

int DBN::getCategoryCount() const{
    return classificationCategories;
}

/**
 * @brief Recoginze a list of images.
 *
 * Every row of the input matrix represents an image. The image is first up-propagated to the hidden layer of the network.
 * Then a few gibbs samling iterations is done on the last hidden layer. On every iteration the activations of the category
 * nerurons are summed and the category with the maximal summed value is chosen.
 *
 * @param image input images.
 * @return list of recognized categories.
 */
std::vector<int> DBN::recognize(const Matrix &image) const{
    //Up propagate and start sampling.
    Matrix data=upPropagate(image);
    auto sampler=layers[layerCnt-1]->startSampling(data,true);

    //Do some sampling and sum the output of the category neurons.
    Matrix accu=sampler->sample(0);
    for (int a=0; a<2; a++){
        accu+=sampler->sample(1);
    }

    //Search for the maximal activation for every output row.
    //Then push the category to the output list.
    const int startIndex=accu.cols()-classificationCategories;

    std::vector <int> result;
    const int cnt=image.rows();

    for (int a=0; a<cnt; a++){
        //Find maximally probable class.
        int cat=0;
        float max=accu(a, startIndex+cat);

        for (int c=1; c<classificationCategories; c++){
            float curr=accu(a, startIndex+c);
            if (curr>max){
                max=curr;
                cat=c;
            }
        }

        result.push_back(cat);
    }

    return result;
}
