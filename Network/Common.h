#ifndef COMMON_H
#define COMMON_H

#include "Matrix.h"

class Common
{
private:
    Common();
public:
    static Matrix logistic(const Matrix &mat);
    static Matrix randomSample(const Matrix &mat);
};

#endif // COMMON_H
