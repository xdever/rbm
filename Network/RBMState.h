#ifndef RBMSTATE_H
#define RBMSTATE_H

#include "Matrix.h"

class RBMState{
    friend class RBM;
    friend class RBMVisualizer;
    friend class DBN;
    private:
        Matrix weights;
        Matrix visibleBias;
        Matrix hiddenBias;
        int nVisible;
        int nHidden;

        float weightDecay;
        float momentum;
        float sparsity;
        float sparsityAverageCoeff;
        float sparsityCost;

    public:
        RBMState(const std::string filename);
        RBMState(std::ifstream &file);
        RBMState(int nVisible, int nHidden, RBMState const *oldState=NULL);
        RBMState& operator=(const RBMState& other);

        void save(const std::string filename) const;
        void load(const std::string filename);

        void save(std::ofstream &file) const;
        void load(std::ifstream &file);
};

#endif // RBMSTATE_H
