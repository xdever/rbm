#ifndef TESTTHREAD_H
#define TESTTHREAD_H

#include <QThread>
#include "Database.h"
#include "GenerativeNetwork.h"

class TrainThread:public QThread
{
    Q_OBJECT
public:
    TrainThread(GenerativeNetwork *network, Database *db);
    ~TrainThread();

    void stop();
    bool isRunning();
    void train(int batchSize=20, float learningRate=0.1, int nInRow=10, int weightImageW=28, int imageInterval=3);
signals:
    void stateInfo(std::vector<QImage>, int, float);
private:
    GenerativeNetwork *network;
    Database *db;
    int batchSize;
    float learningRate;
    int imageInterval;
    int nInRow;
    int weightImageW;

    int round;

    bool running=true;
    void run();
};

#endif // TRAINTHREAD_H
