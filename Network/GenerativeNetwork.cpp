#include "GenerativeNetwork.h"

std::shared_ptr<GenerativeNetworkSampler> GenerativeNetwork::startCategorySampling() const{
    std::vector<int> categories;
    for (int a=0; a<getCategoryCount(); a++){
        categories.push_back(a);
    }

    return startCategorySampling(categories);
}
