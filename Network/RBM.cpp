#include <iostream>
#include <functional>
#include "RBM.h"
#include <math.h>
#include <fstream>
#include <random>
#include "RBMVisualizer.h"
#include "Common.h"


RBM::RBM(int nVisible, int nHidden, RBMState const *initialState):
    state(nVisible, nHidden, initialState){

    if (initialState){
        resetTrainTemporaryState();
    } else {
        initState();
    }
}

/**
 * @brief Initialize the state of the RBM to a starting value.
 *
 */
void RBM::initState(){
    initWeightMatrix();
    initBiases();
    resetTrainTemporaryState();
}

/**
 * @brief Reset biases of the network.
 *
 */
void RBM::initBiases(){
    state.hiddenBias=Matrix::Zeros(1,state.nHidden);
    state.visibleBias=Matrix::Zeros(1,state.nVisible);
}

/**
 * @brief Intialize weight matrix to random weights.
 *
 */
void RBM::initWeightMatrix(){
    //Geoffrey Hinton suggests intializing weights from normal distribution
    //with mean of 0 and standard deviation of 0.01
    std::default_random_engine generator;
    generator.seed(time(0));
    std::normal_distribution<float> distribution(0.0,0.01);

    for (int v=0; v<state.nVisible; v++){
        for (int h=0; h<state.nHidden; h++){
            state.weights(v,h)=distribution(generator);
        }
    }
}


RBM::~RBM()
{

}

/**
 * @brief Propagate activation from visible to hidden units.
 *
 * NOTE: Hinton suggests not to sample the probability of the visible units.
 * Instead, use the actual probabilities in the "forward" step.
 *
 * @param data the activation matrix of the visible layer.
 * @return the activation matrix of the hidden layer.
 */
Matrix RBM::vis2hid(const Matrix &data) const{
    return Common::logistic((data * state.weights).rowwiseAdd(state.hiddenBias));
}

/**
 * @brief Propagates activation from hidden to visible units. It also
 *        samples the state of the hidden units.
 *
 * @param data the activation matrix of the hidden layer.
 * @return the activation matrix of the visible layer.
 */
Matrix RBM::hid2vis(const Matrix &data) const{
    return Common::logistic((Common::randomSample(data) * state.weights.transpose()).rowwiseAdd(state.visibleBias));
}

/**
 * @brief Modify a history matrix with a given difference matrix.
 *        It uses the RBM's global momentum configuration.
 *
 * @param newval the new difference value
 * @param oldval the value of the last update
 * @return reference to oldval
 */
Matrix &RBM::calcMomentum(const Matrix &newval, Matrix &oldval) const{
    oldval=oldval*state.momentum+newval*(1.0-state.momentum);
    return oldval;
}

/**
 * @brief Do a single training step.
 *
 * @param data input data matrix. It should have nVisible columns and every
 *             row is an input data vector.
 * @param CDn the number of Contrastive Divergence steps. The number of
 *            sampling steps to do.
 * @param learningRate learning rate.
 * @return reconstruction error from the first backward step.
 */
float RBM::train(const Matrix &data, int CDn, float learningRate){
    Matrix firstHiddenProbability, newVisibleProb, errorReference;
    //Calculate the first step hidden activation
    Matrix newHiddenProb = firstHiddenProbability = vis2hid(data);

    //Do Gibbs sampling CDn times.
    for (int i=0; i<CDn; i++){
        newVisibleProb=hid2vis(newHiddenProb);
        newHiddenProb=vis2hid(newVisibleProb);

        //Save first visible reconstruction for error calculation
        if (i==0)
            errorReference=newVisibleProb;
    }

    //Weight correction
    calcMomentum(learningRate * (data.transpose() * firstHiddenProbability - newVisibleProb.transpose() * newHiddenProb)/data.rows(), lastWeightCorrection);
    state.weights += lastWeightCorrection;

    //Weight decay
    state.weights *= (1.0f - learningRate * state.weightDecay);

    //Update biases with momentum
    calcMomentum(learningRate * (data-newVisibleProb).meanOfRows(), lastVisisbleBiasCorrection);
    calcMomentum(learningRate * (firstHiddenProbability-newHiddenProb).meanOfRows(), lastHiddenBiasCorrection);

    state.visibleBias += lastVisisbleBiasCorrection;
    state.hiddenBias += lastHiddenBiasCorrection;

    //Sparsity
    meanActivity = meanActivity * (state.sparsityAverageCoeff) + firstHiddenProbability.meanOfRows()*(1.0-state.sparsityAverageCoeff);
    Matrix sparsityCorrection = learningRate * state.sparsityCost * (meanActivity - state.sparsity);

    //state.weights.rowwiseSubtract(sparsityCorrection, true);
    //state.hiddenBias.rowwiseSubtract(sparsityCorrection, true);

    //printf("Sparsity correction [0] was: %f\r\n", sparsityCorrection(0,0));

    //Calculate error
    return (data - errorReference).elementwise([](float d){
            return d*d;
        }).sumOfRows().elementwise(::sqrtf).mean();
}

/**
 * @brief Create a minibatch from database and call a single training step on it.
 *
 * @param db sample database.
 * @param batchSize number of samples in the minibatch.
 * @param learningRate learning rate.
 * @param cd number of sampling steps to do
 * @return reconstruction error from the first backward step.
 */
float RBM::trainStep(const Database &db, int batchSize, float learningRate, int cd){
    int catCount=db.categoryCount();
    DatabaseSample data=db.getSampleFromAllCategories((batchSize+catCount-1)/catCount);

    return train(data.imageData, cd, learningRate);
}

 std::shared_ptr<GenerativeNetworkSampler> RBM::startSampling(const Matrix &data, bool startWithHidden) const{
    RBMSampler *sampler=new RBMSampler(this);

    if (startWithHidden){
        sampler->state=hid2vis(data);
    } else {
        sampler->state=data;
    }

    return  std::shared_ptr<GenerativeNetworkSampler> (sampler);
}


/**
 * @brief Get the extremes of the weight matrix.
 *
 * @param min output. The minimal weight.
 * @param max output. The maximal weight.
 */
void RBM::weightMinMax(float &min, float &max) const{
    state.weights.minMax(min, max);
}


/**
 * @brief Reset the temporary training state of the network.
 *        This includes momemntum calculation buffers, etc.
 */
void RBM::resetTrainTemporaryState(){
    lastWeightCorrection=Matrix::Zeros(state.nVisible,state.nHidden);
    lastVisisbleBiasCorrection=Matrix::Zeros(1, state.nVisible);
    lastHiddenBiasCorrection=Matrix::Zeros(1, state.nHidden);
    meanActivity=Matrix::Zeros(1, state.nHidden);
}

int RBM::getVisibleSize() const{
    return state.nVisible;
}

int RBM::getHiddenSize(int layer) const{
    (void)layer;
    return state.nHidden;
}

void RBM::save(const std::string filename) const{
    state.save(filename);
}


void RBM::load(const std::string filename){
    state=RBMState(filename);
    resetTrainTemporaryState();
}

std::vector<QImage> RBM::plotWeights(int nInRow, int weightImageW) const{
    return RBMVisualizer::plotWeights(*this, nInRow, weightImageW);
}

int RBM::getLayerCount() const{
    return 1;
}

RBMSampler::RBMSampler(const RBM *owner){
    this->owner=owner;
}

RBMSampler::RBMSampler(const RBMSampler &other){
    this->owner=other.owner;
    this->state=other.state;
}

RBMSampler::~RBMSampler(){

}

Matrix RBMSampler::sample(int nSteps){
    for (int step=0; step<nSteps; step++){
        state=owner->hid2vis(owner->vis2hid(state));
    }

    return state;
}

int RBMSampler::layerSize() const{
    return owner->getVisibleSize();
}

std::shared_ptr<GenerativeNetworkSampler> RBM::startCategorySampling(const std::vector<int> &categoryList) const{
    throw "RBM doesn't supports category sampling.";
}

int RBM::getCategoryCount() const{
    return 0;
}

std::vector<int> RBM::recognize(const Matrix &image) const{
    throw "RBM doesn't support classification.";
}
