#include "Common.h"
#include "math.h"

Common::Common()
{

}

/**
 * @brief Applies logistic function for every element of a matrix.
 *
 * @param mat the input matrix
 * @return output matrix.
 *
 */
Matrix Common::logistic(const Matrix &mat){
    return mat.elementwise([](float val){
        return 1.0f/(1.0f + expf(-val));
    });
}

/**
 * @brief Sample a probability matrix. Generates random binary sampes based
 *        on given probabilities
 *
 * @param mat a porbability matrix
 * @return a sampled binary matrix
 */
Matrix Common::randomSample(const Matrix &mat){
    return mat.elementwise([](float probability){
        return (((float)rand())/((float)RAND_MAX)) <= probability ? 1.0f : 0.0f;
    });
}
