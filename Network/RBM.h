#ifndef RBM_H
#define RBM_H

#include "Matrix.h"
#include "Database.h"
#include <QImage>

/**
 * @brief Restricted Boltzmann Machine implementation
 *
 * NOTE: The implementation uses matrix arithmetic in order to speed up
 * computation. It is not necessary to use matrices in an RBM implementaiton.
 *
 */

#include "RBMState.h"
#include "GenerativeNetwork.h"

class RBM;

class RBMSampler: public GenerativeNetworkSampler{
    private:
        friend class RBM;

        const RBM *owner;
        Matrix state;
    public:
        RBMSampler(const RBM *owner);
        RBMSampler(const RBMSampler &other);

        virtual ~RBMSampler();
        virtual Matrix sample(int nSteps);

        int layerSize() const;
};

class RBM: public GenerativeNetwork
{
private:
    friend class RBMVisualizer;
    friend class DBN;
    friend class RBMSampler;

    RBMState state;

    //For the momentum
    Matrix lastWeightCorrection;
    Matrix lastVisisbleBiasCorrection;
    Matrix lastHiddenBiasCorrection;
    //For sparsity
    Matrix meanActivity;

    float train(const Matrix &data, int CDn=1, float learningRate=0.1);



    Matrix vis2hid(const Matrix &data) const;
    Matrix hid2vis(const Matrix &data) const;

    Matrix &calcMomentum(const Matrix &newval, Matrix &oldval) const;

    void resetTrainTemporaryState();

    void initWeightMatrix();
    void initBiases();
public:
    RBM(int nVisible, int nHidden, RBMState const* initialState=NULL);
    virtual ~RBM();

     std::shared_ptr<GenerativeNetworkSampler> startSampling(const Matrix &data, bool startWithHidden=false) const;

    float trainStep(const Database &db, int batchSize=10, float learningRate=0.1, int cd=1);

    void initState();

    void weightMinMax(float &min, float &max) const;

    int getVisibleSize() const;
    int getHiddenSize(int layer=-1) const;

    void save(const std::string filename) const;
    void load(const std::string filename);

    std::vector<QImage> plotWeights(int nInRow, int weightImageW) const;

    int getLayerCount() const;

    std::shared_ptr<GenerativeNetworkSampler> startCategorySampling(const std::vector<int> &categoryList) const;
    int getCategoryCount() const;

    std::vector<int> recognize(const Matrix &image) const;

};

#endif // RBM_H
