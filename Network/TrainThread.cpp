#include "TrainThread.h"
#include "RBMVisualizer.h"
#include <iostream>

TrainThread::TrainThread(GenerativeNetwork *network, Database *db)
{
    this->network=network;
    this->db=db;
    round=0;
    running=false;
}

void TrainThread::train(int batchSize, float learningRate, int nInRow, int weightImageW, int imageInterval){
    this->batchSize=batchSize;
    this->learningRate=learningRate;
    this->imageInterval=imageInterval;
    this->nInRow=nInRow;
    this->weightImageW=weightImageW;


    running=true;

    start();
}

void TrainThread::stop(){
    running=false;
}

TrainThread::~TrainThread()
{
    stop();
    wait();
}

bool TrainThread::isRunning(){
    return running;
}

void TrainThread::run(){
    while(running){
        float error=network->trainStep(*db, batchSize, learningRate,1);
        round++;

        if (round % imageInterval==0){
            std::vector<QImage> w=network->plotWeights(nInRow, weightImageW);
            emit stateInfo(w, round, error);
        }
    }
}

