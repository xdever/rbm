#ifndef GENERATIVENETWORK
#define GENERATIVENETWORK

#include "Matrix.h"
#include <string>
#include <QImage>
#include <vector>
#include <memory>
#include "Database.h"

/**
 * @brief Generator class for generative networks.
 *
 * It is intended to hold the network state while sampling,
 * and return batch of samples.
 *
 */
class GenerativeNetworkSampler{
    public:
        virtual ~GenerativeNetworkSampler(){};
        virtual Matrix sample(int nSteps)=0;
        virtual int layerSize() const=0;
};

/**
 * @brief Neural network that can generate data.
 *
 */
class GenerativeNetwork{
    public:
        virtual ~GenerativeNetwork(){};
        virtual float trainStep(const Database &db, int batchSize=10, float learningRate=0.1, int cd=1)=0;

        virtual std::shared_ptr<GenerativeNetworkSampler> startSampling(const Matrix &data, bool startWithHidden=false) const=0;
        virtual std::shared_ptr<GenerativeNetworkSampler> startCategorySampling(const std::vector<int> &categoryList) const=0;
        std::shared_ptr<GenerativeNetworkSampler> startCategorySampling() const;

        virtual int getVisibleSize() const=0;
        virtual int getHiddenSize(int layer=-1) const=0;
        virtual int getCategoryCount() const=0;

        virtual int getLayerCount() const=0;

        virtual void initState()=0;

        virtual void save(const std::string filename) const=0;
        virtual void load(const std::string filename)=0;

        virtual std::vector<QImage> plotWeights(int nInRow, int weightImageW) const=0;

        virtual std::vector<int> recognize(const Matrix &image) const=0;
};

#endif // GENERATIVENETWORK

